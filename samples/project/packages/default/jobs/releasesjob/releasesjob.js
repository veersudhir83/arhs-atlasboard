/*
	Releases Job
 */

var request = require('request');
var URLParser = require('../../service/urlparser').URLParser;

module.exports = function(config, dependencies, job_callback) {
		
	var urlObject = new URLParser(config.jiraUrl,"/rest/api/2/project/"+config.projectCode+"/versions");
	var options = {
		url : urlObject.getNormalizedUrl('https'),
		auth: {
				'user': config.user,
				'pass': config.password				
			}
	};

	request.get(options, function(err, res, body) {

		var url = "<a href='"+options.url+"'>"+options.url+"</a>";
		
		if (err){ return job_callback("Could not find Jira at "+ url); }
		if (!err && res.statusCode == 200) {		
			var nextReleaseInfo = getNextRelease(JSON.parse(body));
			nextReleaseInfo.warningThreshold = config.warningThreshold;
		    nextReleaseInfo.criticalThreshold = config.criticalThreshold;
			job_callback(null,nextReleaseInfo);
			
		} else {
			if(res.statusCode == 404 || res.statusCode == 500 ){
				job_callback("Error " + res.statusCode + ": "+ JSON.parse(res.body).errorMessages);
			} else if(res.statusCode == 403 || res.statusCode == 401 ){
				var loginUrl = new URLParser(config.jiraUrl).getNormalizedUrl('https')+"/login.jsp";
				var loginUrlLink = "<a href='"+loginUrl+"'>"+loginUrl+"</a>";
				job_callback("Error " + res.statusCode + ": Check your credentials. If the problem persists, try to login manually via " + loginUrlLink);
			} else{
				job_callback("Error JIRA " + res.statusCode);
			}						
			
		}
		
    });
	
};

function getNextRelease(releases){

	var currentDate = getCurrentDate();
	var nextReleaseDate = currentDate;
	var nextRelease;
	var first = false; // first release after the current date has been encountered
	releases.forEach(function(release){
		if(release.releaseDate){
			var releaseDate = getMidnightDate(new Date(release.releaseDate));
			if(releaseDate >= currentDate){			
				if(!first){
					nextReleaseDate = releaseDate;
					nextRelease = release;
					first = true;
				}
				// if release is closer 
				if(releaseDate < nextReleaseDate){
					nextReleaseDate = releaseDate;
					nextRelease = release;
				}
			}
			
		}
		
	});	

	if(nextRelease){
		return { nbDaysLeft : getNbDaysBetween(nextReleaseDate,getCurrentDate()), nextReleaseName : nextRelease.name};
	}	
	return {};
}


function getMidnightDate(date){
	var midnightDate = date.setHours(0,0,0,0);
	return midnightDate;
}

function getCurrentDate(){
	return getMidnightDate(new Date());
}

function getNbDaysBetween(date1,date2){
	return Math.floor((date1 - date2)/(1000 * 60 * 60 * 24));
}






