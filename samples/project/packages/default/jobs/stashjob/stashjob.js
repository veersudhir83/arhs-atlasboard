/*

  Git job.

 */

var request = require('request');
var CAS = require('../../service/cas').CAS;
var URLParser = require('../../service/urlparser').URLParser;


module.exports = function(config, dependencies, job_callback) {

	var urlObject = new URLParser(config.stash_server,"/rest/api/1.0/projects/" + config.project + "/repos/" + config.repo + "/commits?until=" +  encodeUrlComponent(config.branch) +"&limit=5");
	var options = {
	  url : urlObject.getNormalizedUrl('https'),
	  auth: {
				'user': config.user,
				'pass': config.password
			  }
	};

	request.get(options,
	function(err, res, body) {
  
		var url = "<a href='"+options.url+"'>"+options.url+"</a>";
		
		if (err){ return job_callback("Could not find Stash server at: "+ url); }
		if (!err && res.statusCode == 200) {
			
			job_callback(null,JSON.parse(body));
		
		}  else {
			if(res.statusCode == 404 || res.statusCode == 500){	
		
				var errorJson = JSON.parse(res.body);
				job_callback("Error " + res.statusCode + ": "+ errorJson.errors[0].message);
			} else if(res.statusCode == 403 || res.statusCode == 401){
				job_callback("Error " + res.statusCode + ": Check your credentials and access rights");
			} else{
				job_callback("Error STASH " + res.statusCode);
			}						
			
		}
		
        
	});


};

function encodeUrlComponent(url){
	if(url.indexOf('%') != -1) {
        return url;
    }

    return encodeURIComponent(url);
}