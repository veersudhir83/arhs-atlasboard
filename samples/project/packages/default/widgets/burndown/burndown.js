widget = {

  onData: function(el, data) {
  
  
    //change the widget title if custom data is sent
    if (data.title) {
      $('.widget-title', el).text(data.title);
    }
	

	
	$(el).find('.burndownchart').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        xAxis: {
			title: {
                text: 'Time',
				style: {
                    fontSize:'15px'
                },				
            },
			type : 'datetime',
			tickInterval : 24 * 3600 * 1000 ,
			labels: {
                style: {
                    fontSize:'15px'
                }
            },
			plotLines: [{
				color: 'red', 
				dashStyle: 'longdashdot', 
				value: Date.now(), 
				width: 2  
			}]
        },
        yAxis: {
            title: {
                text: 'Original Time Estimate ( h )',
				style: {
                    fontSize:'15px'
                },				
            },
			min: 0,
			labels: {
                style: {
                    fontSize:'15px'
                }
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'horizontal',
            align: 'right',
            verticalAlign: 'top',
            borderWidth: 0,
			itemStyle: {
				fontSize: '15px'
			}
        },
        series: [{
			step : 'left',
            name: 'Remaining values',
            data: data[0]
        },{
            name: 'Guideline',
            data: data[1]
        }]
    });	
	
	}
	
};

